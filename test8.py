import numpy as np
np.random.seed(0)
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers import Dense,Activation,Convolution2D,MaxPooling2D,Flatten,Dropout
from keras.utils import np_utils
import matplotlib.pyplot as plt

data=[]
image=[]
original_label=[]
DATA_COUNT=12
input_size = 784
batch_size = 100
hidden_neuron = 200
classes = 10
epochs = 15
(X_train,Y_train),(X_test,Y_test) = mnist.load_data()
X_train  = X_train.reshape(60000,28,28,1)
X_test = X_test.reshape(10000,28,28,1)
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /=255
Y_train = np_utils.to_categorical(Y_train,classes)
Y_test = np_utils.to_categorical(Y_test,classes)
model = Sequential()
model.add(Convolution2D(32,(3,3),input_shape=(28,28,1)))
model.add(Activation('sigmoid'))
model.add(Convolution2D(32,(3,3)))
model.add(Activation('sigmoid'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(Dense(hidden_neuron))
model.add(Activation('sigmoid'))
model.add(Dense(classes))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy',metrics=['accuracy'],optimizer='adadelta')
model.fit(X_train,Y_train,batch_size=batch_size,epochs=epochs,verbose=1,validation_split=0.1)
score = model.evaluate(X_train,Y_train,batch_size=batch_size,verbose=1)
print 'Train accuracy:',score[1]
score = model.evaluate(X_test,Y_test,verbose=1)
print 'Test accuracy:',score[1]
for i in range(DATA_COUNT):
    x=X_test[np.random.randint(0,len(X_test))]
    y=Y_test[np.random.randint(0,len(X_test))]
    im = x.reshape(28,28)
    plt.imshow(im)
    plt.show()
    data.append(x)
    image.append(im)
    original_label.append(np.argmax(y))
preds = model.predict(np.array(data),batch_size=DATA_COUNT)
labels = np.argmax(preds,axis=1)
print "Original label:",original_label
print "Predicted label:",labels

