import numpy as np
np.random.seed(0)
from keras.layers import Dense,LSTM,Flatten
from keras.datasets import imdb
from keras.models import Sequential
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
top_words = 5000
max_words = 500
classes = 10
batch_size = 128
epochs = 8
(x_train,y_train),(x_test,y_test) = imdb.load_data(num_words=top_words)
x_train = x_train.reshape(25000,32,32)
x_test = x_test.reshape(25000,32,32)
y_train = y_train.to_categorical(y_train,classes)
y_test = y_test.to_categorical(y_test,classes)
x_train = sequence.pad_sequences(x_train,maxlen=max_words)
x_test = sequence.pad_sequences(x_test,maxlen=top_words)
model = Sequential()
model.add(Embedding(top_words,32,input_length=max_words))
model.add(Flatten())
model.add(Dense(250,activation='relu'))
model.add(Dense(1,activation='sigmoid'))
model.compile(optimizer='adam',loss='binary_crossentropy',metrics=['accuracy'])
model.fit(x_train,y_train,batch_size=batch_size,validation_data=(x_test,y_test),verbose=1)
score=model.evaluate(x_test,y_test,batch_size=128)
print 'Test accuracy:',score[1]
