from keras.models import Sequential
from keras.layers.core import Dense,Activation
from keras.datasets import mnist
from keras.utils import np_utils
import numpy as np
from PIL import Image

(X_train,Y_train),(X_test,Y_test) = mnist.load_data()
input_size = 784
hidden_neuron = 100
epochs = 10
batch_size=100
classes = 10
X_train = X_train.reshape(60000,784)
X_test = X_test.reshape(10000,784)
Y_train = np_utils.to_categorical(Y_train,classes)
Y_test = np_utils.to_categorical(Y_test,classes)
model = Sequential()
model.add(Dense(hidden_neuron,input_dim=input_size))
model.add(Activation('sigmoid'))
model.add(Dense(classes,input_dim=hidden_neuron))
model.add(Activation('softmax'))
model.compile(loss='categorical_crossentropy',metrics=['accuracy'],optimizer='sgd')
model.fit(X_train,Y_train,batch_size,nb_epoch=epochs,verbose=1)
data = model.predict(X_test,batch_size=batch_size,verbose=1)
im = Image.fromarray(data)
im.save('out.jpeg')