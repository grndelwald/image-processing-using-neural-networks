import numpy as np
np.random.seed(7)
from keras.datasets import cifar10
from keras.layers import Dense,Dropout,Convolution2D,MaxPooling2D,Activation,Flatten
from keras.models import Sequential
from keras.utils import np_utils
import matplotlib.pyplot as plt


input_size = 1024
hidden_size = 400
classes = 10
batch_size = 100
epochs = 10
data =[]
image = []
original_label = []
predicted_label = []
count = 5

(xtrain,ytrain),(xtest,ytest)=cifar10.load_data()
xtrain = xtrain.reshape(50000,32,32,3)
xtest = xtest.reshape(10000,32,32,3)
ytrain = np_utils.to_categorical(ytrain,classes)
ytest = np_utils.to_categorical(ytest,classes)
model = Sequential()
model.add(Convolution2D(32,(3,3),input_shape=(32,32,3)))
model.add(Activation('relu'))
model.add(Convolution2D(36,(3,3)))
model.add(Activation('relu'))
model.add(Dropout(0.25))
model.add(Flatten())
model.add(MaxPooling2D(pool_size=(4,4)))
model.add(Dense(hidden_size))
model.add(Activation('sigmoid'))
model.add(Dense(hidden_size))
model.add(Activation('sigmoid'))
model.add(Dropout(0.5))
model.add(Flatten())
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dense(classes))
model.add(Activation('sigmoid'))
model.compile(loss="categorical_crossentropy",metrics=['accuracy'],optimizer='sgd')
model.fit(xtrain,ytrain,batch_size=batch_size,epochs=epochs,verbose=1)
score = model.evaluate(xtrain,ytrain,verbose=1)
print "Traian accuracy:",score[1]
score = model.evaluate(xtest,ytest,verbose=1)
print "Test Accuracy:",score[1]
for i in  range(1,5):
    x = xtest[np.random.randint(0,len(xtest))]
    y= xtest[np.random.randint(0,len(xtest))]
    im = x.reshape(32,32)
    plt.imshow(x)
    plt.show()
    data.append(x)
    image.append(im)
    original_label.append(np.argmax(y))

preds = model.predict(np.array(data),batch_size=5)
labels = np.argmax(preds,axis=1)
print "Original label:",original_label
print "Predicted Label:",labels